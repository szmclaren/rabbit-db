/**
 * @(#) LifecycledAdapter.java RabbitDB
 */
package cn.rabbitdb.faced.server;

/**
 * @author 智慧工厂@
 *
 */
public class LifecycledAdapter implements Lifecycle {

	/*
	 * (non-Javadoc)
	 * 
	 * @see cn.rabbitdb.faced.server.Lifecycle#startup()
	 */
	@Override
	public void startup() {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cn.rabbitdb.faced.server.Lifecycle#shutdown()
	 */
	@Override
	public void shutdown() {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cn.rabbitdb.faced.server.Lifecycle#restart()
	 */
	@Override
	public void restart() {
		this.shutdown();
		this.startup();
	}

}
