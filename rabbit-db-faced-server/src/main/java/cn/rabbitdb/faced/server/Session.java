/**
 * @(#) Session.java RabbitDB
 */
package cn.rabbitdb.faced.server;

import io.netty.channel.ChannelHandlerContext;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

/**
 * @author 智慧工厂@
 *
 */
@RequiredArgsConstructor
@Getter
public class Session {
	private final ChannelHandlerContext ioContext;
	private final byte[] authPluginDataPart1;
	private final byte[] authPluginDataPart2;
	@Setter
	private boolean isAuthorized = false;
}
