/**
 * @(#) DefaultFacedServer.java RabbitDB
 */
package cn.rabbitdb.faced.server;

import cn.rabbitdb.faced.server.configure.ServerConfig;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.epoll.EpollEventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;

/**
 * 默认的前端服务器
 * 
 * @author 智慧工厂@
 *
 */
public class DefaultFacedServer extends AbstractFacedServer {
	/**
	 * @param serverConfig
	 */
	public DefaultFacedServer(ServerConfig serverConfig) {
		super(serverConfig.getPort());
	}

	/*
	 * Linux用epoll,其他用NIO
	 */
	@Override
	protected EventLoopGroup getEventLoop() {
		String osName = System.getProperty("os.name");
		if (osName.equalsIgnoreCase("linux")) {
			return new EpollEventLoopGroup();
		}
		return new NioEventLoopGroup();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * cn.rabbitdb.faced.server.AbstractFacedServer#getChannelInboundHandlerAdapter(
	 * )
	 */
	@Override
	protected ChannelInboundHandlerAdapter getChannelInboundHandlerAdapter() {
		return new DefaultChannelInboundHandlerAdapter(this);
	}

}
