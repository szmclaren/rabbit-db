/**
 * @(#) Test.java RabbitDB
 */
package cn.rabbitdb.faced.server;

import java.io.InputStream;
import java.net.Socket;

import lombok.Cleanup;
import lombok.SneakyThrows;

/**
 * @author 智慧工厂@
 *
 */
public class Test {

	/**
	 * @param args
	 */
	@SneakyThrows
	public static void main(String[] args) {
		@Cleanup
		Socket socket = new Socket("127.0.0.1",3306);
		InputStream in = socket.getInputStream();
		int k = -1;
		while((k = in.read()) != -1) {
			System.out.print(k+",");
		}
	}

}
